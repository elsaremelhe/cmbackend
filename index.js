//Imports
var mongodb = require('mongodb');
var objectID = mongodb.ObjectID;
var crypto = require('crypto');
var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');


//PSWD Utils

//FUNCS to random salt

var genRandomString = function(length){
    return crypto.randomBytes(Math.ceil(length/2)).toString('hex').slice(0,length);
};

var sha512 = function(pswd, salt){
    var hash = crypto.createHmac('sha512', salt);
    hash.update(pswd);
    var value = hash.digest('hex');
    
    return{
        salt: salt,
        pswdHash: value
    };
};

function saltHashPswd(userPswd){
    var salt = genRandomString(16); //Create 16 random characters
    var pswdData = sha512(userPswd, salt);
    return pswdData;
};

function checkHashPswd(userPswd, salt){
    var pswdData = sha512(userPswd, salt);
    return pswdData;
}


//Express service
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//MongoDB Client
var mongoClient = mongodb.MongoClient;

//Connection URL
var url = 'mongodb://localhost:27017' //default port

mongoClient.connect(url, {useNewUrlParser: true}, function(err, client){
    if(err){
        console.log('Unable to connect to DB server. Error: ', err);
    }
    else{ //Start WS ada

        //Register user
        app.post('/register', (request, response, next)=>{
            var postData = request.body;

            var plaintPswd = postData.pswd;
            var hashData = saltHashPswd(plaintPswd);

            var pswd = hashData.pswdHash; //save pswd hash
            var salt = hashData.salt; //save salt

            var name = postData.name;
            var email = postData.email;
            
            var insertJson = {
                'email': email,
                'pswd': pswd,
                'salt': salt,
                'name': name
            };

            var db = client.db('cmproject');

            //check if email exists
            db.collection('user').find({'email': email})
            .toArray()
            .then(responsi => {
                console.log(responsi)
                if(responsi.length != 0){
                    console.log("CHEGUEI")
                    response.json('Email already registered');
                    console.log('Email already registered');
                } else{
                    console.log("JUST STOP")
                    //Insert data
                    db.collection('user').insertOne(insertJson, function(error, res){
                        response.json('Registered successfully');
                        console.log('Registered successfully');
                    })
                }
            })
            .catch(errori  => {

            })

        });

        app.post('/login', (request, res, next)=>{

            var postData = request.body;
            var email = postData.email;
            var userPswd = postData.pswd;

            var db = client.db('cmproject');

            //check if email exists
            db.collection('user')
            .find({'email': email})
            .toArray()
            .then(function(respostinha){
                console.log(respostinha);
                if(respostinha.length == 0){
                    res.json('Email not registered');
                    return;
                } 

                var user = respostinha[0];

                console.log(user);
                    
                var salt = user.salt; //get salt from user
                var hashedPswd = checkHashPswd(userPswd, salt).pswdHash; //hash pswd with salt
                var encryptedPswd = user.pswd; //get pswd from user


                if(hashedPswd == encryptedPswd){
                    res.json({
                        status: true,
                        message: {
                            idUser: user._id,
                            name: user.name,
                            loginMessage: 'Loged in successfully'
                        }
                    });
                    console.log('Loged in successfully');
                } else{
                    res.json({
                        status: false,
                        message:"Wrong credentials"
                    });
                    console.log('Wrong credentials');
                }
                
            }).catch(erroris =>{
                console.log(erroris);
                res.json("Um erro");
            })

        });

        
         app.post('/addmarker', (request, response, next)=>{

                var data = request.body;
                //No pedido vais passar o id do user e o nome
                //acrescentas variaveiss para essas duas
                var desc = data.desc;
                var img = data.img;
                var idUser = data.idUser;
                var name = data.name;
                console.log("Image saved")
                var lat = data.lat;
                var long = data.long;
    
    
                var insertJson={
                    'idUser': idUser,
                    'nameUser': name,
                    'desc': desc,
                    'img': img,
                    'lat':lat,
                    'long':long
                }
    
                var db = client.db('cmproject');
    
                db.collection('marker').insert(insertJson, (error,res)=>{
                    response.json('marker added c:');
                    console.log('marker added c:');
                })
    
            });

        app.get('/getmarkers', (request, response)=>{

            var db = client.db('cmproject');

            db.collection('marker').find({}).toArray().then(resposta =>{
                if(resposta.length == 0){
                    response.json("No markers to show");
                    console.log("No markers to show");
                } else{
                    response.json(resposta);
                }

            }).catch(erro =>{
                console.log(erro);
            })
        })

        app.post('/getmarkersbyuser', (request, response, next)=>{

            var data = request.body;
            var idUser = data.idUser;

            console.log("Inicio:"+idUser+":fim");

            var db = client.db('cmproject');

            db.collection('marker').find({"idUser": idUser}).toArray().then(resposta =>{
                console.log(resposta);
                if(resposta.length == 0){
                    response.json("No markers to show");
                    console.log("No markers to show");
                } else{
                    response.json(resposta);
                }

            }).catch(erro =>{
                console.log(erro);
            })
        })

        app.post('/deletemarker', (request, response, next)=>{

            var postData = request.body;
            var id = postData.id;

            var db = client.db('cmproject');

            db.collection('marker').deleteOne({"_id": new mongodb.ObjectId(id)}).then(resposta =>{
                if(resposta.length == 0){
                    response.json("erro");
                    console.log("erro");
                } else{
                    response.json(resposta);
                }

            }).catch(erro =>{
                console.log(erro);
            })

        })

        app.post('/editmarker', (request, response, next)=>{

            var data = request.body;
            var id = data.id;
            var desc = data.desc;
            var img = data.img;

            var db = client.db('cmproject');

            db.collection('marker').updateOne({"_id": new mongodb.ObjectId(id)},
            {$set:{"desc": desc, "img": img}}).then(resposta =>{
                if(resposta.length == 0){
                    response.json("erro");
                    console.log("erro");
                } else{
                    response.json(resposta);
                    console.log(resposta.result);
                }

            }).catch(erro =>{
                console.log(erro);
            })

        });

        app.listen(3000, () =>{
            console.log('Connected to DB server');
        })
    }
});

